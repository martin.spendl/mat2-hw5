% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
  twocolumn]{article}
\title{Markov Chain Monte Carlo (HW5)}
\author{Martin Špendl}
\date{9/3/2022}

\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Markov Chain Monte Carlo (HW5)},
  pdfauthor={Martin Špendl},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\begin{document}
\maketitle

\hypertarget{implementations}{%
\subsection{Implementations}\label{implementations}}

We have implemented 3 methods for sampling from a target distribution:
Rejection sampling with uniform envelope (RS), Metropolis-Hastings
algorithm (MH) and Hamiltonian Monte Carlo with Leapfrog method (HMC).

\hypertarget{bivariate-normal-distribution}{%
\subsection{Bivariate normal
distribution}\label{bivariate-normal-distribution}}

Bivariate standard normal distribution is a joint distribution of two
standard normal distribution, thus having
\(\mu = \begin{pmatrix} 0 \\ 0 \end{pmatrix}\) and
\(\sum = \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}\).

\begin{figure}
\centering
\includegraphics{hw5_report_files/figure-latex/mvn.trace-1.pdf}
\caption{MC algorithm comparison for approximation of bivariate standard
normal distribution. Colors represent different runs of the algorithm.}
\end{figure}

Rejection sampling performed well with no tuning, because we already
knew the distribution. We used sampled on the interval \([-10,10]\) for
each component, however, if estimating the mean of the distribution, we
could even shrink the interval. Even better option would be to use
bivariate distribution to sample from in the first place, thus making
the envelope the same as the target distribution. RS has perfect ESS as
we sample IID samples from uniform distribution, thus having 0
autocovariance. Also, when we reject a sample, we do not add the
previous one as with other algorithms. This makes adding samples slower,
hence lower ESS/s.

Metropolis-Hastings also performed decent with no tuning, again, because
of our knowledge of the target distribution. It had low autocovariance,
thus high ESS and high ESS/s.

Hamiltonian Monte Carlo was more difficult to tune, as it had the
tendency to drift from the mean. Using first guess of \(\epsilon = 0.5\)
and \(L = 50\), we achieved localized and autocorrelated samples, thus
achieving low ESS. Tuning parameters to lower values (0.2 and 25,
respectively), we achieved more disperse samples and much higher ESS.

\begin{table}[h!]
\centering
\begin{tabular}{l|rrrr}
Algorithm & \multicolumn{2}{c}{ESS} & \multicolumn{2}{c}{ESS/s} \\
 & x1 & x2 & x1 & x2 \\
\hline
RS & 1000 & 1000 & 181 & 181 \\
MH & 57.9 & 87.2 & 205.8 & 309.5 \\
HMC & 11.2 & 4 & 1.2 & 0.4 \\
Tuned HMC & 584 & 621.3 & 152.8 & 162.6 \\
\hline
\end{tabular}
\caption{Effective sample size comparison between algorithms per simulated chain.}
\end{table}

All algorithms performed well on estimating the mean of the
distribution, however, this estimation is biased, because we chose the
mean as a starting position for MH and HMC.

\begin{table}[h!]
\centering
\begin{tabular}{l|rr}
Algorithm & x1 & x2 \\
\hline
RS & -0.012 & 0.017 \\
MH & 0.023 & 0.008 \\
HMC & 0.218 & 0.031 \\
Tuned HMC & -0.005 & -0.026 \\
\hline
\end{tabular}
\caption{Estimates of mean with different algorithms.}
\end{table}

\newpage

\hypertarget{banana-distribution}{%
\subsection{Banana distribution}\label{banana-distribution}}

Banana distribution is distribution given as a \(f(x) = -log(p(x))\)
transformed function by professor. To use it in RS and MH algorithms, we
had to transform it (\(p(x) = exp(-f(x))\)). Because we did not know the
distribution, we had to guess the initial parameters. We estimated by
guessing that \(T(0,5)\) was the mode of the distribution and used the
evaluation for M parameter in RS.

\begin{figure}
\centering
\includegraphics{hw5_report_files/figure-latex/bnn.trace-1.pdf}
\caption{MC algorithm comparison for approximation of banana
distribution. Colors represent different runs of the algorithm.}
\end{figure}

With RS and tuned RS we observe that we initially limited the space too
much with uniform sampling, however, this sampling was time-wise the
most efficient. If we would not know the mode of the distribution,
tuning parameters would be more difficult. Using default parameters for
MH and \((0,0)\) as a starting point gives us decent feeling about the
shape of the banana, however, ESS and mean estimation were poor. Tuning
parameters was hard, because the best proposal is not the same in each
tail and in the bulk part of the distribution. HMC has the advantage of
being able to cross long distances of the distribution in one step, thus
achieving the best results. Without tuning we got a good estimate of the
mean and high ESS compared to MH. Tuning improved those results by
adjusting for longer leaps. Tunning mass matrix did not improve those
results.

\begin{table}[h!]
\centering
\begin{tabular}{l|rrrr}
Algorithm & \multicolumn{2}{c}{ESS} & \multicolumn{2}{c}{ESS/s} \\
 & x1 & x2 & x1 & x2 \\
\hline
RS & 1000 & 1000 & 8273.2 & 8273.2 \\
Tuned RS & 1000 & 1000 & 1994.3 & 1994.3 \\
MH & 6.7 & 5.7 & 64.9 & 55.1 \\
Tuned MH & 23.1 & 8.4 & 263.1 & 95.6 \\
HMC & 250.3 & 202 & 1159.3 & 935.7 \\
Tuned HMC & 1167.2 & 344.6 & 5051 & 1491.1 \\
\hline
\end{tabular}
\caption{Effective sample size comparison between algorithms per simulated chain.}
\end{table}

Despite recognizable distributions by MH traces, they did not provide a
sufficient estimation of the mean. MH struggled to get samples from the
tails of the distribution, thus having a bias for a positive mean of x2
coordinate. HMC was the best performing algorithm and relatively easy to
tune.

\begin{table}[h!]
\centering
\begin{tabular}{l|rr}
Algorithm & x1 & x2 \\
\hline
RS & -0.04 & 3.49 \\
Tuned RS & 0.07 & 0.44 \\
MH & 3.48 & 2.5 \\
Tuned MH & 3.26 & 2.74 \\
HMC & 0.03 & 0.4 \\
Tuned HMC & -0.08 & -0.09 \\
\hline
\end{tabular}
\caption{Estimates of mean with different algorithms.}
\end{table}

\includegraphics{hw5_report_files/figure-latex/banana.acf-1.pdf}
\includegraphics{hw5_report_files/figure-latex/banana.acf-2.pdf}

From autocorrelation plots we can observe that MH has high
autocorrelation and shows local behavior, whereas HMC moves globally and
is less autocorrelated. For this type of distributions, HMC is the best
type of sampling algorithm.

\hypertarget{logistic-regression-likelihood}{%
\subsection{Logistic regression
likelihood}\label{logistic-regression-likelihood}}

Likelihood of logistic regression is denoted by
\[L = \prod^n_{i=1}p_i^{y_i}(1-p_i)^{1-y_i}\], where
\[p_i = \frac{1}{1+exp(w^Tx_i)}.\]

Estimating likelihood would be hard, as the product gets smaller with
samples and becomes numerically unstable. For RS and MH algorithms, we
estimated loglikelihood, which translates to a sum. For MLE estimation,
the log transformation does not affect the mode of the distribution,
however, it becomes less steep. We have also added 2000 to the value, so
that it became positive.

In MHC we use \(-log(f(x))\) transformed density estimation, where
negative loglikelihood is the correct input for likelihood estimation.
This allows us to generate samples from likelihood density directly.

\begin{figure}
\centering
\includegraphics{hw5_report_files/figure-latex/lr.trace-1.pdf}
\caption{MC algorithm comparison for approximation of likelihood. Colors
represent different runs of the algorithm.}
\end{figure}

Logarithmic transformation of likelihood smoothens the distribution,
thus making uniform sampling biased. We cannot really compare the
results between themselves, because this is a new distribution.

The most applicable algorithm is HMC, which can sample from the
likelihood directly. We can observe that the initial parameters, where
we started at \((0,0)\) with \(\epsilon=0.001\) and \(L = 25\).
Trajectories of all 5 chains are pointing towards bottom right, thus to
the mode of the distribution. We know the maximum of the likelihood from
MLE, which is (1.37, -0.7).

\begin{table}[h!]
\centering
\begin{tabular}{l|rrrr}
Algorithm & \multicolumn{2}{c}{ESS} & \multicolumn{2}{c}{ESS/s} \\
 & x1 & x2 & x1 & x2 \\
\hline
RS & 1076.6 & 1000 & 482.6 & 448.3 \\
Tuned RS & 1000 & 944.5 & 192.1 & 181.5 \\
MH & 20.9 & 23.1 & 92.9 & 102.5 \\
Tuned MH & 1.6 & 1.5 & 6.7 & 6.5 \\
HMC & 1.6 & 1.3 & 1 & 0.8 \\
Tuned HMC & 17.1 & 15 & 10.1 & 8.9 \\
\hline
\end{tabular}
\caption{Effective sample size comparison between algorithms per simulated chain.}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{l|rr}
Algorithm & x1 & x2 \\
\hline
RS & 4.55 & -3.01 \\
Tuned RS & 4.47 & -3 \\
MH & 3.74 & -2.49 \\
Tuned MH & 1.61 & -1.24 \\
HMC & 0.15 & -0.07 \\
Tuned HMC & 1.37 & -0.72 \\
\hline
\end{tabular}
\caption{Estimates of mean with different algorithms.}
\end{table}

\hypertarget{multiple-parameters}{%
\subsubsection{Multiple parameters}\label{multiple-parameters}}

We are now considering the whole data set with 10 features and the
intercept. We have trained a logistic regression classifier on the whole
data set to compare trained weights with HMC estimates.

High dimensional distribution requires more samples for a good
estimation, however, using 5 chains with 1000 samples, we can only start
to navigate a general direction for the MC. Choosing different
parameters is tricky, because too large step-size can quickly lead to
lower acceptance.

\begin{table}[h!]
\centering
\begin{tabular}{ll|rr}
Parameter & Estimation & HMC1 & HMC2 \\
\hline
X1 & 2.048 & 0.133 &  0.042 \\
X2 & -0.871 & -0.058 &  0.023 \\
X3 & -0.499 & -0.024 &  0.01 \\
X4 & 0.717 & 0.018 &  0.021 \\
X5 & -0.084 & -0.005 &  -0.015 \\
X6 & -1.019 & -0.062 &  -0.066 \\
X7 & -0.819 & -0.001 &  0.003 \\
X8 & 0.24 & -0.014 &  0.034 \\
X9 & -0.605 & -0.048 &  -0.006 \\
X10 & -0.406 & -0.023 &  0.02 \\
X11 & 0.478 & 0.004 &  0.022 \\
\hline
\end{tabular}
\caption{Estimates of logistic regression parameters.}
\end{table}

We chose two scenarios. The first with parameters \(\epsilon = 0.0001\)
and \(L=25\) and the second \(\epsilon = 0.00005\) and \(L=200\). We
chose a large L because we know that mean of the distribution is not at
0, thus we want to explore out as quickly as possible.

In general, HMC is sufficient for estimating likelihood, however, 1000
samples and 5 chains is not suitable to characterize a multi dimensional
distribution. Informative choice of a starting position can help
immensely with HMC exploration.

\end{document}
